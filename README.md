# Street Racer
Es un juego similar al Street Racen de Atari 2600.

## Actores
1. El auto del jugador que se controla por teclado.
2. Enemigos: Hay dos tipos de autos enemigos distintos que se diferencian cambiando los colores.
   El auto amarillo cae en sentido vertical manteniendo su posición
   El auto rojo cae azarozamente moviénse a los lados (puede dirigirse hacia el auto del jugador, pero  no es una regla necesaria).
3. Un obstáculo fijo que cae al igual que el auto amarillo.

## Reglas del juego
Por cada auto sobrepasado se suma 1 punto. Al llegar a 20 se sube de nivel y se aumenta la velocidad. Así hasta los 20, 40, 60, etc.

## Caracteristica técnica
El juego está escrito en C++ con la libreria conio2.h
Se adjunta el archivo zpr para compilarlo rapidamente con el IDE zinjai.
